package ca.csf.mobile2.revsision_exam.ViewModel

import androidx.databinding.BaseObservable
import androidx.databinding.Observable
import ca.csf.mobile2.revsision_exam.Model.QuestionData
import org.parceler.Parcel
import org.parceler.ParcelConstructor

@Parcel(Parcel.Serialization.BEAN)
class ViewModel @ParcelConstructor constructor(questionData: QuestionData) : BaseObservable(){
    var questionData : QuestionData = QuestionData()


    val titleText : String
        get() = questionData.text

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        super.addOnPropertyChangedCallback(callback)

        questionData.addChangeListener(this::notifyChange)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        super.addOnPropertyChangedCallback(callback)

        questionData.removeChangeListener(this::notifyChange)
    }
}