package ca.csf.mobile2.revsision_exam

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import ca.csf.mobile2.revsision_exam.Model.QuestionData
import ca.csf.mobile2.revsision_exam.Service.QuestionService
import ca.csf.mobile2.revsision_exam.ViewModel.ViewModel
import ca.csf.mobile2.revsision_exam.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import org.androidannotations.annotations.*

@SuppressLint("Registered")
@DataBound
@EActivity(R.layout.activity_main)
class MainActivity : AppCompatActivity() {

    @BindingObject
    protected lateinit var binding: ActivityMainBinding

    @Bean
    protected lateinit var questionService : QuestionService

    @InstanceState
    protected lateinit var viewModel: ViewModel

    @InstanceState
    protected lateinit var questionData: QuestionData

    @AfterViews
    protected fun onCreate() {
        if (!this::viewModel.isInitialized){
            questionData = QuestionData()
            viewModel = ViewModel(questionData)
        }
        binding.viewModel = viewModel
        questionService = QuestionService()
        questionService.getQuestion(this::onSuccess,this::onServerError,this::onConnectionError)
    }

    private fun onSuccess(questionData: QuestionData){
        textView.text = questionData.text
    }

    private fun onConnectionError(){
        textView.text = "connection error"
    }

    private fun onServerError(){
        textView.text = "server error"
    }
}
