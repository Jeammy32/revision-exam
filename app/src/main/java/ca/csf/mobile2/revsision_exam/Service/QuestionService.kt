package ca.csf.mobile2.revsision_exam.Service

import ca.csf.mobile2.revsision_exam.Model.QuestionData
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.androidannotations.annotations.Background
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.UiThread
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import java.lang.Exception


@EBean(scope = EBean.Scope.Singleton)
class QuestionService {
    private val service: Service

    init {
        var retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
            .build()

        service = retrofit.create()
    }

    @Background
    fun getQuestion(
        onSuccess: (QuestionData) -> Unit,
        onServerError: () -> Unit,
        onConnectionError: () -> Unit
    ) {
        try {
            val response = service.getQuestion().execute()
            if (response.isSuccessful) {
                val result = response.body()
                doInUIThread { onSuccess(result!!) }
            } else {
                doInUIThread { onServerError() }
            }
        } catch (e: Exception) {
            doInUIThread { onConnectionError() }
        }
    }

    @UiThread
    fun doInUIThread(callback: () -> Unit) {
        callback()
    }

    private interface Service {
        @GET("/api/v1/question/random")
        fun getQuestion(): Call<QuestionData>
    }
}

private const val URL = "http://192.168.2.22:8080"