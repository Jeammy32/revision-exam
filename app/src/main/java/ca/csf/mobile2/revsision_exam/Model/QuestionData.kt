package ca.csf.mobile2.revsision_exam.Model

import android.view.OrientationEventListener
import org.parceler.Parcel
import org.parceler.ParcelConstructor

@Parcel(Parcel.Serialization.BEAN)
class QuestionData @ParcelConstructor constructor(
    choice1 : String,
    choice2 : String,
    id : String,
    nbChoice1 : Int,
    nbChoice2 : Int,
    text : String
){
    var text = text
        set(value) {
            field = value
            notifyChanged()
        }
    var choice1 = choice1
        set(value) {
            field = value
            notifyChanged()
        }
    var choice2 = choice2
        set(value) {
            field = value
            notifyChanged()
        }
    var id = id
        set(value) {
            field = value
            notifyChanged()
        }
    var nbChoice1 = nbChoice1
        set(value) {
            field = value
            notifyChanged()
        }
    var nbChoice2 = nbChoice2
        set(value) {
            field = value
            notifyChanged()
        }

    private val changeListeners: MutableList<() -> Unit> = mutableListOf()

    constructor() : this("","","",0,0,"")

    fun addChangeListener(listener: () -> Unit){
        addChangeListener(listener)
    }

    fun removeChangeListener(listener: () -> Unit){
        removeChangeListener(listener)
    }

    private fun notifyChanged(){
        if (changeListeners.size > 0){
            for (listener in changeListeners) listener()
        }
    }
}